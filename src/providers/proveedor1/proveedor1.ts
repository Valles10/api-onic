import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class Proveedor1Provider {

  constructor(private http: HttpClient) { }

  getDatos(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/users');
  }
}