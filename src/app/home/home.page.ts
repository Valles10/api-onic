import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Proveedor1Provider } from 'src/providers/proveedor1/proveedor1';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  results: Observable<any>;

  constructor(private proveedor: Proveedor1Provider) {
    this.results = this.proveedor.getDatos();
  }

  ngOnInit() {
  }

}